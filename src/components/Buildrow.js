import React, { Component } from 'react'

class Buildrow extends Component {
    render() {
        return (
            <>
            <tr>
                <td>{this.props.name}</td>
                <td>{this.props.total}</td>
            </tr>

        </>
        )
    }
}

export default Buildrow
