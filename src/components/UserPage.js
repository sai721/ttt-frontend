import React, { Component } from 'react'
import axios from 'axios'
import Buildrow from './Buildrow'

import { Button } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { Table } from 'react-bootstrap';

class UserPage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             totalData: [],
             specifiedData: [],
             number: '',
             show: false,
        }
    }
    componentDidMount(){
        axios.get("https://3.128.18.231:9000/result")
        .then(response => {
            
            this.setState({totalData: response.data})
        })
        .catch(err => {
            console.log(err);
        })
    }
    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value, show: false})
    }
    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({ show: true});
       

    }
    render() {
        const {totalData,specifiedData,number, show}=this.state;
        return (
            <div className="form">
                
                <br/>
                <br/>
                

                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Control type="text" name="number" value={number} onChange={this.handleChange} placeholder="Enter Number"/>
                    </Form.Group>

                    
                    
                    <Button variant="primary" type="submit" onClick={this.handleSubmit}>
                        Submit
                    </Button>
                </Form>
                <br/>
                {show && (
                    <Table striped bordered hover size="sm">
                    <thead>
                      <tr>
                        <th>Word</th>
                        <th>Count</th>
                      </tr>
                    </thead>
                    <tbody>{totalData.slice(0,number).map((row,index) => 
                    <Buildrow key={index} name={row.name} total={row.total}/>)}</tbody>
                </Table>)}

            </div>
        )
    }
}

export default UserPage
