import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Userpage from "./components/UserPage";
function App() {
  return (
    <div className="App">
      <Userpage />
    </div>
  );
}

export default App;
